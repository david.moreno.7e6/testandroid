package com.example.androidtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.androidtest.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {
    lateinit var binding: FragmentFirstBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        binding= FragmentFirstBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var nombres= arrayOf("mario","david","sergio")
        arguments?.getString("used").toString()
        binding.next.setOnClickListener {
            val nom= nombres.random()
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(nom)
            findNavController().navigate(action)
        }
        binding.rank.setOnClickListener {
            findNavController().navigate(R.id.action_firstFragment_to_rankFragment)
        }


    }
}