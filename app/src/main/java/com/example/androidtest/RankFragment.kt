package com.example.androidtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidtest.databinding.FragmentRankBinding
import java.io.BufferedReader
import java.io.InputStreamReader

class RankFragment : Fragment() {
    lateinit var binding: FragmentRankBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        binding= FragmentRankBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readFile()
    }
    fun readFile() {
        var content = mutableListOf<String>()
        val file = InputStreamReader(requireActivity().openFileInput("names.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content.add(line)
            line = br.readLine()
        }
        var nombres= arrayOf("mario","david","sergio")
        var final= mutableListOf<MutableList<String>>()
        for (i in nombres){
            var reps=0
            for (j in content){
                if (i == j){
                    reps++
                }
            }
            final.add(mutableListOf(i, "$reps"))
        }
        final.sortByDescending { it[1].toInt() }
        var text=""
        for (p in final){
            text+= p[0] + " "+ p[1] + "\n"
        }
        binding.result.text = text
    }

}