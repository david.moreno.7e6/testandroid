package com.example.androidtest

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.androidtest.databinding.FragmentSecondBinding
import java.io.OutputStreamWriter

class SecondFragment : Fragment() {

    lateinit var binding: FragmentSecondBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        binding= FragmentSecondBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val used= arguments?.getString("nom").toString()
        fileExists("names.txt")
        writeFile(used)
        binding.nom.text= used
        binding.back.setOnClickListener {
            val back= SecondFragmentDirections.actionSecondFragmentToFirstFragment(used)
            findNavController().navigate(back)
        }
    }
    fun fileExists(file:String): Boolean{
        val files = requireActivity().fileList()
        val exists = files.find { it == file }
        if(exists!=null) return true
        return false
    }
    fun writeFile(value: String){
        val file = OutputStreamWriter(requireActivity().openFileOutput("names.txt", Activity.MODE_APPEND))
        file.appendLine(value)
        file.flush()
        file.close()
    }
}